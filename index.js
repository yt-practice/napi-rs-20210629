const { loadBinding } = require('@node-rs/helper')

/**
 * __dirname means load native addon from current dir
 * 'napi-rs-20210629' is the name of native addon
 * the second arguments was decided by `napi.name` field in `package.json`
 * the third arguments was decided by `name` field in `package.json`
 * `loadBinding` helper will load `napi-rs-20210629.[PLATFORM].node` from `__dirname` first
 * If failed to load addon, it will fallback to load from `napi-rs-20210629-[PLATFORM]`
 */
module.exports = loadBinding(__dirname, 'napi-rs-20210629', 'napi-rs-20210629')
